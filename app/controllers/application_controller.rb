class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # A controller action
  def hello
    render html: "hello, world!"
  end
end
